/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payrolapp;

/**
 *
 * @author GHOST
 */
public class Employee {

    /**
     * @return the unions
     */
    public String getUnions() {
        return unions;
    }

    /**
     * @param unions the unions to set
     */
    public void setUnions(String unions) {
        this.unions = unions;
    }

    /**
     * @return the funeralSocieties
     */
    public String getFuneralSocieties() {
        return funeralSocieties;
    }

    /**
     * @param funeralSocieties the funeralSocieties to set
     */
    public void setFuneralSocieties(String funeralSocieties) {
        this.funeralSocieties = funeralSocieties;
    }

    /**
     * @return the salaryNO
     */
    public Integer getSalaryNO() {
        return salaryNO;
    }

    /**
     * @param salaryNO the salaryNO to set
     */
    public void setSalaryNO(Integer salaryNO) {
        this.salaryNO = salaryNO;
    }

    /**
     * @return the hiredDate
     */
    
    private String firstName;
    private String surName;
    private String hiredDate;
    private String dob;
    private int id;
    private String gender;
    private Double basicSallary;
    private Double hilauDimana;
    private String designation;
    private Integer lifeNo;
    private Double galapumDimana;
    private String unions;
    private String funeralSocieties;
    private Integer salaryNO;
    public Employee(
                String firstName, 
                String surName, 
                String dob,
                String gender, 
                Double basicSallary,
                String hiredDate,
                String designation,
                Double hilauDimana,
                Integer lifeNo,
                Double galapumDimana,
                String unions,
                String funeralSocieties, 
                Integer salaryNO
        )
    {
        this.firstName = firstName;
        this.surName = surName;
        this.gender = gender;
        this.dob = dob;
        this.basicSallary = basicSallary;
        this.hiredDate = hiredDate;
        this.hilauDimana = hilauDimana;
        this.designation = designation;
        this.lifeNo = lifeNo;
        this.galapumDimana = galapumDimana;
        this.salaryNO= salaryNO;
        this.unions = unions;
        this.funeralSocieties = funeralSocieties;
        this.unions = unions;
    }
    public Employee(int id,
                String firstName, 
                String surName, 
                String dob,
                String gender, 
                Double basicSallary,
                String hiredDate,
                String designation,
                Double hilauDimana,
                Integer lifeNo,
                Double galapumDimana,
                String unions,
                String funeralSocieties, 
                Integer salaryNO
        )
    {
        this.firstName = firstName;
        this.surName = surName;
        this.gender = gender;
        this.dob = dob;
        this.id= id;
        this.basicSallary = basicSallary;
        this.hiredDate = hiredDate;
        this.hilauDimana = hilauDimana;
        this.designation = designation;
        this.lifeNo = lifeNo;
        this.galapumDimana = galapumDimana;
        this.salaryNO = salaryNO;
        this.funeralSocieties = funeralSocieties;
        this.unions = unions;
    }
    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the surName
     */
    public String getSurName() {
        return surName;
    }

    /**
     * @param surName the surName to set
     */
    public void setSurName(String surName) {
        this.surName = surName;
    }

    /**
     * @return the id
     */
   
    /**
     * @return the id
     */

    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the basicSallary
     */
    public Double getBasicSallary() {
        return basicSallary;
    }

    /**
     * @param basicSallary the basicSallary to set
     */
    public void setBasicSallary(Double basicSallary) {
        this.basicSallary = basicSallary;
    }
    public String getHiredDate() {
        return hiredDate;
    }

    /**
     * @param hiredDate the hiredDate to set
     */
    public void setHiredDate(String hiredDate) {
        this.hiredDate = hiredDate;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the hilauDimana
     */
    public Double getHilauDimana() {
        return hilauDimana;
    }

    /**
     * @param hilauDimana the hilauDimana to set
     */
    public void setHilauDimana(Double hilauDimana) {
        this.hilauDimana = hilauDimana;
    }

    /**
     * @return the designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * @param designation the designation to set
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * @return the life_no
     */
    public Integer getLife_no() {
        return lifeNo;
    }

    /**
     * @param life_no the life_no to set
     */
    public void setLife_no(Integer life_no) {
        this.lifeNo = life_no;
    }

    /**
     * @return the galapumDimana
     */
    public Double getGalapumDimana() {
        return galapumDimana;
    }

    /**
     * @param galapumDimana the galapumDimana to set
     */
    public void setGalapumDimana(Double galapumDimana) {
        this.galapumDimana = galapumDimana;
    }
}
